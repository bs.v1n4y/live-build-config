# Building Kali Linux 
------------------------------------
## Getting Ready the kali linux
```bash
sudo apt update
sudo apt install -y git live-build simple-cdd cdebootstrap curl
```

## To build the installer image
```bash
./build.sh --verbose --installer
```
